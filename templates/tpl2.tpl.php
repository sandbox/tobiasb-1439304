PREFIX dbp2: <http://dbpedia.org/ontology/>
SELECT ?abstract ?thumbnail ?leader ?population ?postalCode ?area ?lat ?long
WHERE {
?id rdfs:label "<?php echo $term; ?>"@de .
?id dbp2:abstract ?abstract .
?id dbp2:thumbnail ?thumbnail .
OPTIONAL {
    ?id dbp2:leader ?leaderid .
    ?leaderid rdfs:label ?leader .
} .
OPTIONAL { ?id dbp2:areaTotal ?area } .
OPTIONAL { ?id dbp2:populationTotal ?population } .
OPTIONAL { ?id dbp2:postalCode ?postalCode } .
OPTIONAL { ?id geo:lat ?lat } .
OPTIONAL { ?id geo:long ?long } .
FILTER langMatches(lang(?abstract), 'de')
FILTER langMatches(lang(?leader), 'de')
}