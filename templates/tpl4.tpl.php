PREFIX dbp: <http://dbpedia.org/resource/>
PREFIX dbp2: <http://dbpedia.org/ontology/>
PREFIX dbpprop: <http://dbpedia.org/property/>
SELECT ?abstract ?thumbnail ?formationDate ?formationPlace ?foundedBy ?keyPerson ?numberOfEmployees ?homepage
WHERE {
?id rdfs:label "<?php echo $term; ?>"@de .
?id dbp2:abstract ?abstract .
?id dbp2:thumbnail ?thumbnail .
OPTIONAL { ?id dbp2:formationDate ?formationDate } .
OPTIONAL { ?id dbp2:formationPlace ?formationPlace } .
OPTIONAL { ?id dbp2:foundedBy ?foundedBy } .
OPTIONAL { ?id dbp2:keyPerson ?keyPerson } .
OPTIONAL { ?id dbpprop:numberOfEmployees ?numberOfEmployees } .
OPTIONAL { ?id dbpprop:homepage ?homepage } .
FILTER langMatches(lang(?abstract), 'de')
}