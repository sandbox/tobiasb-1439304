PREFIX dbp2: <http://dbpedia.org/ontology/>
SELECT ?abstract ?thumbnail ?leader ?capital ?lat ?long
WHERE {
?id rdfs:label "<?php echo $term; ?>"@de .
?id dbp2:abstract ?abstract .
?id dbp2:thumbnail ?thumbnail .
OPTIONAL {
    ?id dbp2:leaderName ?leaderid .
    ?leaderid rdfs:label ?leader .
} .
OPTIONAL { ?id dbp2:areaTotal ?area } .
OPTIONAL {
  ?id dbp2:capital ?capitalid .
  ?capitalid rdfs:label ?capital
} .
OPTIONAL { ?id dbp2:currency ?currency } .
OPTIONAL { ?id geo:lat ?lat } .
OPTIONAL { ?id geo:long ?long } .
FILTER langMatches(lang(?abstract), 'de') .
FILTER langMatches(lang(?capital), 'de')
FILTER langMatches(lang(?leader), 'de')
}