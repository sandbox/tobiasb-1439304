PREFIX dbp2: <http://dbpedia.org/ontology/>
SELECT ?abstract ?thumbnail ?bday ?bcity
WHERE {
  ?id rdfs:label "<?php echo $term; ?>."@de .
  ?id dbp2:abstract ?abstract .
  ?id dbp2:thumbnail ?thumbnail .
  OPTIONAL { ?id dbp2:birthDate ?bday } .
  OPTIONAL {
    ?id dbp2:birthPlace ?bcityid .
    ?bcityid rdfs:label ?bcity .
  } .
  FILTER langMatches(lang(?abstract), 'de')
}