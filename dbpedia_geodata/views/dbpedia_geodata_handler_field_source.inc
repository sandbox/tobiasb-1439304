<?php

class dbpedia_geodata_handler_field_source extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['source'] = 'source';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $value = $values->{$this->aliases['source']};
    return _dbpedia_geodata_source_output($value);
  }
}
