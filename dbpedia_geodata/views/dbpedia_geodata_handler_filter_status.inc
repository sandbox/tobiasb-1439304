<?php


class dbpedia_geodata_handler_filter_status extends views_handler_filter_in_operator  {
  function get_value_options() {
   if (!isset($this->value_options)) {
      $this->value_options = _dbpedia_geodata_status_options();
    }
  }
}
