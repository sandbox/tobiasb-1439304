<?php
/**
 * @file
 * Providing extra functionality for the Geodata UI via views.
 */


/**
 * Implements hook_views_data()
 */
function dbpedia_geodata_views_data_alter(&$data)  {
  $table = 'dbpedia_geodata';
  $data[$table]['status']['filter']['handler'] = 'dbpedia_geodata_handler_filter_status';
  $data[$table]['status']['field']['handler'] = 'dbpedia_geodata_handler_field_status';
  $data[$table]['type']['filter']['handler'] = 'dbpedia_geodata_handler_filter_type';
  $data[$table]['source']['field']['handler'] = 'dbpedia_geodata_handler_field_source';
  $data[$table]['view_geodata'] = array(
    'field' => array(
      'title' => t('View Link'),
      'help' => t('Provide a link to the view for the geodata.'),
      'handler' => 'dbpedia_geodata_handler_view_link_field',
    ),
  );
  $data[$table]['edit_geodata'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the geodata.'),
      'handler' => 'dbpedia_geodata_handler_edit_link_field',
    ),
  );
  $data[$table]['delete_geodata'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the geodata.'),
      'handler' => 'dbpedia_geodata_handler_delete_link_field',
    ),
  );
  return $data;
}