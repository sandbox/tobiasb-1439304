<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying links to entities
 * as fields.
 */

class dbpedia_geodata_handler_view_link_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['id'] = 'id';
    $this->additional_fields['name'] ='name';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $name = $values->{$this->aliases['name']};
    $text = !empty($this->options['text']) ? $this->options['text'] : $name;
    $id = $values->{$this->aliases['id']};
    return l($text, 'admin/content/geodata/' . $id);
  }
}
