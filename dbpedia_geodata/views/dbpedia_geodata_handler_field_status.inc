<?php

class dbpedia_geodata_handler_field_status extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['status'] = 'status';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $value = $values->{$this->aliases['status']};
    $options = _dbpedia_geodata_status_options();
    return isset($options[$value]) ? $options[$value] : $value;
  }
}
