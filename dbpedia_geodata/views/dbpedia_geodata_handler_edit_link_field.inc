<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying links to entities
 * as fields.
 */

class dbpedia_geodata_handler_edit_link_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['id'] = 'id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }


  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
    $id = $values->{$this->aliases['id']};

    return l($text, 'admin/content/geodata/' . $id . '/edit');
  }
}
