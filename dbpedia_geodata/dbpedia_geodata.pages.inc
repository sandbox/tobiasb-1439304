<?php

/**
 * Add new geodata page callback.
 */
function dbpedia_geodata_add() {
  $geodata = entity_create('dbpedia_geodata', array());
  $output = drupal_get_form('dbpedia_geodata_form', $geodata);
  return $output;
}

/**
 * Geodata form.
 */
function dbpedia_geodata_form($form, &$form_state, $geodata) {
  $form_state['geodata'] = $geodata;
  $form['type'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#title' => t('Type'),
    '#options' => array('Stadt' => 'Stadt', 'Land' => 'Land'),
    '#default_value' => $geodata->type,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name of this geodata'),
    '#default_value' => $geodata->name,
  );
  $form['lat'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Lat',
    '#default_value' => $geodata->lat,
  );
  $form['lng'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Long',
    '#default_value' => $geodata->lng,
  );
  $form['source'] = array(
    '#type' => 'select',
    '#options' => dbpedia_geodata_source_options(),
    '#title' => 'Source',
    '#default_value' => $geodata->source,
  );
  $form['status'] = array(
    '#type' => 'select',
    '#title' => 'Status',
    '#options' => _dbpedia_geodata_status_options(),
    '#default_value' => $geodata->status,
    '#requiired' => TRUE,
  );
  field_attach_form('dbpedia_geodata', $geodata, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }
  $form['actions'] = array(
    '#weight' => 100,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('dbpedia_geodata_form_submit'),
  );
  // Show Delete button if we edit task.
  $geodata_id = entity_id('dbpedia_geodata', $geodata);
  if (!empty($geodata_id) && dbpedia_geodata_access('edit', $geodata)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('dbpedia_geodata_form_submit_delete'),
    );
  }
  return $form;
}

/**
 * Task submit handler.
 */
function dbpedia_geodata_form_submit($form, &$form_state) {
  $geodata = $form_state['geodata'];
  entity_form_submit_build_entity('dbpedia_geodata', $geodata, $form, $form_state);
  dbpedia_geodata_save($geodata);
  $geodata_uri = entity_uri('dbpedia_geodata', $geodata);
  $form_state['redirect'] = $geodata_uri['path'];
  drupal_set_message(t('Geodata %title saved.', array('%title' => entity_label('dbpedia_geodata', $geodata))));
}

function dbpedia_geodata_form_submit_delete($form, &$form_state) {
  $geodata = $form_state['geodata'];
  $geodata_uri = entity_uri('dbpedia_geodata', $geodata);
  $form_state['redirect'] = $geodata_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function dbpedia_geodata_delete_form($form, &$form_state, $geodata) {
  $form_state['geodata'] = $geodata;
  $geodata_uri = entity_uri('dbpedia_geodata', $geodata);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => entity_label('dbpedia_geodata', $geodata))),
    $geodata_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

  /**
   * Delete confirmation form.
   */
function dbpedia_geodata_delete_form_submit($form, &$form_state) {
  $geodata = $form_state['geodata'];
  dbpedia_geodata_delete($geodata);
  drupal_set_message(t('%title deleted.', array('%title' => entity_label('dbpedia_geodata', $geodata))));
  $form_state['redirect'] = 'admin/content/geodata';
}

/**
 * Task view callback.
 */
function dbpedia_geodata_view($geodata) {
  drupal_set_title(entity_label('dbpedia_geodata', $geodata));
  return entity_view('dbpedia_geodata', array(entity_id('dbpedia_geodata', $geodata) => $geodata), 'full');
}

/**
 * Collects all valid geodata for the source form element.
 *
 * @return array
 * An numeric array (geodata ids) of geodata with his name as value.
 */
function dbpedia_geodata_source_options() {
  $matches = array(NULL => '--Keine Quelle--');
  $query = db_select('dbpedia_geodata', 'dg')
  ->fields('dg', array('id', 'name'))
  ->condition('dg.status', 1);
  $result = $query->execute();
  foreach ($result as $geodata) {
    $matches[$geodata->id] = $geodata->name;
  }
  return $matches;
}