<?php

/**
 * @param $type
 * -city
 * -land
 * @return array
 * An numeric array of cities or lands.
 */
function hook_dbpedia_geodata($type) {
  if ($type == 'land') {
    return array('Österreich','Deutschland');
  }
  elseif ($type == 'city') {
    return array('Berlin', 'Moskau');
  }
  return array();
}