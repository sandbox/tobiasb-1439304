<?php
/**
 * DBPediaGeodata class.
 */
class DBPediaGeodata extends Entity {
  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'admin/content/geodata/' . $this->identifier());
  }
}

class DBPediaGeodataController extends EntityAPIController {

  public function create(array $values = array()) {
    $values += array(
      'type' => '',
      'name' => '',
      'lat' => 0,
      'lng' => 0,
      'source' => NULL,
      'status' => -1,
    );
    return parent::create($values);
  }
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
    $header = array('Property', 'Value');
    $rows = array();
    $status = _dbpedia_geodata_status_options();
    $rows[] = array('id', $entity->id);
    $rows[] = array('type', $entity->type);
    $rows[] = array('name', $entity->name);
    $rows[] = array('lat', $entity->lat);
    $rows[] = array('long', $entity->lng);
    $rows[] = array('status', $status[$entity->status]);
    $rows[] = array('source', _dbpedia_geodata_source_output($entity->source));
    $content['geodata'] = array(
      '#markup' => theme('table', array('header' =>  $header, 'rows' => $rows)),
    );
    return $content;
  }

 }