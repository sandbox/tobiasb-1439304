<?php
/**
 * @file Contains functions to request dbpedia for all our cities, countries
 * for the lat/long attributes and store the result in the database for
 * later use. (Google Maps))
 */

function _dbpedia_geodata_cron_run() {
  $items = _dbpedia_geodata_get_cron_items();
  $queue = DrupalQueue::get('dbpedia_geodata');
  foreach ($items as $item) {
    $queue->createItem($item);
  }
}

/**
 * Collects the missing countries and cities for the geodata table.
 *
 * @return array
 * An array of coutries and cities database object.
 */
function _dbpedia_geodata_get_cron_items() {
  $items = array();
  $max = 10;
  $last_countries = variable_get('dbpedia_geodata.cron.countries', 0);
  if ((time() - $last_countries) >= 60*60) {
    $countries = module_invoke_all('dbpedia_geodata', 'land');
    if (!empty($countries)) {
      $saved_countries = dbpedia_geodata_load_all('Land');
      foreach ($countries as $country) {
        if (empty($saved_countries[$country]) && $max > 0) {
          $items[$country] = array('name' => $country, 'type' => 'Land');
          $max--;
        }
        elseif ($max < 1) {
          break;
        }
      }
      if (!empty($items)) {
        variable_set('dbpedia_geodata.cron.countries', time());
      }
    }
  }
  $max = 10;
  $last_cities = variable_get('dbpedia_geodata.cron.cities', 0);
  if ((time() - $last_cities) >= 60*60) {
    $cities = module_invoke_all('dbpedia_geodata', 'city');
    if (!empty($cities)) {
      $saved_cities = dbpedia_geodata_load_all('Stadt');
      foreach ($cities as $city) {
        if (empty($saved_cities[$city]) && $max > 0) {
          $items[$city] = array('name' => $city, 'type' => 'Stadt');
          $max--;
        }
        elseif ($max < 1) {
          break;
        }
      }
      if (!empty($items)) {
        variable_set('dbpedia_geodata.cron.cities', time());
      }
    }
  }
  return $items;
}

/**
 * Process one item from the DrupalQueue while cron.
 * @see _dbpedia_geodata_cron_run().
 * @see dbpedia_geodata_cron_queue_info().
 * @param array $item
 */
function dbpedia_geodata_cron_process($item) {
  $path = drupal_get_path('module', 'dbpedia');
  require_once $path . '/dbpedia.class.php';
  $dbpedia = new DBPedia($item['name'], $item['type']);
 # $dbpedia->debug = TRUE;
  $dbpedia->attributes = array('lat', 'long');
  $dbpedia->run();
  $unprepared_data = $dbpedia->unprepared_data;
  if (empty($dbpedia->response_data)) {
    return;
  }
  $geodata = new stdClass();
  $geodata->name = $item['name'];
  $geodata->type = $item['type'];

  if (!empty($unprepared_data)) {
    foreach ($unprepared_data as $attr => $datasets) {
      //multiple values for one cities/land term
      $trust = (count($datasets) > 1) ? FALSE : TRUE;
      foreach ($datasets as $i => $datavalue) {
        if ($attr == 'long') {
          //workaround for the db-columns
          $geodata->lng = $datavalue;
          $geodata->lat = $unprepared_data['lat'][$i];
          $geodata->status = -1;
          if ($trust) {
            $geodata->status =  1;
          }
          dbpedia_geodata_save($geodata);
        }
      }
    }
  }
  else{
    //dbpedia don't know the lat/long
    dbpedia_geodata_save($geodata);
  }
}