<?php
/**
 * @file
 * dbpedia_geodata_list.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dbpedia_geodata_list_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'dbpedia_geodata_overview';
  $view->description = 'Übersicht der gespeicherten Geodaten von Städten und Ländern.';
  $view->tag = 'dbpedia';
  $view->base_table = 'dbpedia_geodata';
  $view->human_name = 'Auflistung der Geodaten';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Alle gespeicherten Geodaten';
  $handler->display->display_options['use_more_text'] = 'mehr';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer geodata entities';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filtern';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Zurücksetzen';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sortieren nach';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Aufsteigend';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Absteigend';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Einträge pro Seite';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 10, 20, 25, 40, 60';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 1;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Alle -';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'view_geodata' => 'view_geodata',
    'type' => 'type',
    'lat' => 'lat',
    'lng' => 'lng',
    'status' => 'status',
    'delete_geodata' => 'edit_geodata',
    'edit_geodata' => 'edit_geodata',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'view_geodata' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'lat' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'lng' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete_geodata' => array(
      'align' => 'views-align-left',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_geodata' => array(
      'align' => 'views-align-left',
      'separator' => ' - ',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Verhalten, wenn keine Ergebnisse vorliegen: Global: Textbereich */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'Keine Geodaten vorhanden.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Feld: Geodata: View Link */
  $handler->display->display_options['fields']['view_geodata']['id'] = 'view_geodata';
  $handler->display->display_options['fields']['view_geodata']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['view_geodata']['field'] = 'view_geodata';
  $handler->display->display_options['fields']['view_geodata']['label'] = 'Name';
  $handler->display->display_options['fields']['view_geodata']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['external'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['view_geodata']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['view_geodata']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['view_geodata']['alter']['html'] = 0;
  $handler->display->display_options['fields']['view_geodata']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['view_geodata']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['view_geodata']['hide_empty'] = 0;
  $handler->display->display_options['fields']['view_geodata']['empty_zero'] = 0;
  $handler->display->display_options['fields']['view_geodata']['hide_alter_empty'] = 1;
  /* Feld: Geodata: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = 'Typ';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
  /* Feld: Geodata: Lat */
  $handler->display->display_options['fields']['lat']['id'] = 'lat';
  $handler->display->display_options['fields']['lat']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['lat']['field'] = 'lat';
  $handler->display->display_options['fields']['lat']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['external'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['lat']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['lat']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['lat']['alter']['html'] = 0;
  $handler->display->display_options['fields']['lat']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['lat']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['lat']['hide_empty'] = 0;
  $handler->display->display_options['fields']['lat']['empty_zero'] = 0;
  $handler->display->display_options['fields']['lat']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['lat']['set_precision'] = 0;
  $handler->display->display_options['fields']['lat']['precision'] = '0';
  $handler->display->display_options['fields']['lat']['format_plural'] = 0;
  /* Feld: Geodata: Lng */
  $handler->display->display_options['fields']['lng']['id'] = 'lng';
  $handler->display->display_options['fields']['lng']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['lng']['field'] = 'lng';
  $handler->display->display_options['fields']['lng']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['external'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['lng']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['lng']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['lng']['alter']['html'] = 0;
  $handler->display->display_options['fields']['lng']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['lng']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['lng']['hide_empty'] = 0;
  $handler->display->display_options['fields']['lng']['empty_zero'] = 0;
  $handler->display->display_options['fields']['lng']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['lng']['set_precision'] = 0;
  $handler->display->display_options['fields']['lng']['precision'] = '0';
  $handler->display->display_options['fields']['lng']['format_plural'] = 0;
  /* Feld: Geodata: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['status']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['status']['hide_alter_empty'] = 1;
  /* Feld: Geodata: Source */
  $handler->display->display_options['fields']['source']['id'] = 'source';
  $handler->display->display_options['fields']['source']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['source']['field'] = 'source';
  $handler->display->display_options['fields']['source']['label'] = 'Quelle';
  $handler->display->display_options['fields']['source']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['source']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['source']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['source']['alter']['external'] = 0;
  $handler->display->display_options['fields']['source']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['source']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['source']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['source']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['source']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['source']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['source']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['source']['alter']['html'] = 0;
  $handler->display->display_options['fields']['source']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['source']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['source']['hide_empty'] = 0;
  $handler->display->display_options['fields']['source']['empty_zero'] = 0;
  $handler->display->display_options['fields']['source']['hide_alter_empty'] = 1;
  /* Feld: Geodata: Edit Link */
  $handler->display->display_options['fields']['edit_geodata']['id'] = 'edit_geodata';
  $handler->display->display_options['fields']['edit_geodata']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['edit_geodata']['field'] = 'edit_geodata';
  $handler->display->display_options['fields']['edit_geodata']['label'] = '';
  $handler->display->display_options['fields']['edit_geodata']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['external'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['edit_geodata']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['edit_geodata']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['alter']['html'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_geodata']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['edit_geodata']['hide_empty'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['empty_zero'] = 0;
  $handler->display->display_options['fields']['edit_geodata']['hide_alter_empty'] = 1;
  /* Feld: Geodata: Delete Link */
  $handler->display->display_options['fields']['delete_geodata']['id'] = 'delete_geodata';
  $handler->display->display_options['fields']['delete_geodata']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['fields']['delete_geodata']['field'] = 'delete_geodata';
  $handler->display->display_options['fields']['delete_geodata']['label'] = '';
  $handler->display->display_options['fields']['delete_geodata']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['external'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['delete_geodata']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['delete_geodata']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['alter']['html'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['delete_geodata']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['delete_geodata']['hide_empty'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['empty_zero'] = 0;
  $handler->display->display_options['fields']['delete_geodata']['hide_alter_empty'] = 1;
  /* Filterkriterium: Geodata: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['name']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['required'] = 0;
  $handler->display->display_options['filters']['name']['expose']['multiple'] = FALSE;
  /* Filterkriterium: Geodata: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'Land' => 'Land',
    'Stadt' => 'Stadt',
  );
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Typ';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['reduce'] = 1;
  /* Filterkriterium: Geodata: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'dbpedia_geodata';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    -1 => '-1',
    1 => '1',
    2 => '2',
  );
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['reduce'] = 1;

  /* Display: Auflistung der Geodaten */
  $handler = $view->new_display('page', 'Auflistung der Geodaten', 'page');
  $handler->display->display_options['display_description'] = 'Auflistung der Geodaten in einer Tabelle';
  $handler->display->display_options['field_language'] = 'und';
  $handler->display->display_options['path'] = 'admin/content/geodata';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Alle gespeicherte Geodaten';
  $handler->display->display_options['menu']['description'] = 'Auflistung der gespeicherten Geodaten für Städte und Länder.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Alle Geodaten';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $translatables['dbpedia_geodata_overview'] = array(
    t('Master'),
    t('Alle gespeicherten Geodaten'),
    t('mehr'),
    t('Filtern'),
    t('Zurücksetzen'),
    t('Sortieren nach'),
    t('Aufsteigend'),
    t('Absteigend'),
    t('Einträge pro Seite'),
    t('- Alle -'),
    t('Offset'),
    t('Keine Geodaten vorhanden.'),
    t('Name'),
    t('Typ'),
    t('Lat'),
    t('.'),
    t(','),
    t('Lng'),
    t('Status'),
    t('Quelle'),
    t('Auflistung der Geodaten'),
    t('Auflistung der Geodaten in einer Tabelle'),
  );
  $export['dbpedia_geodata_overview'] = $view;

  return $export;
}
