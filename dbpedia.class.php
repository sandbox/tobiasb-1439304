<?php

/**
 * @file
 * A simple php-class to fetch data from dbpedia.org.
 *
 * @author Tobias Bähr
 *
 * Usage:
 * @code
 * $dbpedia = new DBPedia('Berlin', 'Stadt');
 * $dbpedia->attributes = array('lat', 'long');
 * $dbpedia->prepare = TRUE;
 * $dbpedia->run();
 * print_r($dbpedia->prepared_data);
 * @endcode
 *
 */

class DBPedia {

  /**
   * Contains response data
   *
   * @var array
   */
  public $response_data = array();

  /**
   * @code
   * $data = array(
   *  'attributekey' => array('NiceValue'),
   * );
   * @endcode
   * @var array
   */
  public $unprepared_data = array();

  /**
   * Contains the prepared data.
   *
   * @code
   * $data = array(
   *  'attributekey' => array(
   *    array('title' => 'Title', 'content' => 'Nice Content'
   *  )),
   * );
   * @endcode
   *
   * @var array
   */
  public $prepared_data = array();

  /**
   * The annotation to search for.
   *
   * @var string
   */
  protected $annotation = '';

  /**
   * The type to search for.
   *
   * @var string
   */
  protected $type = '';

  /**
   * A flag whether logging or not.
   *
   * @var Boolean
   */
  public $debug = FALSE;

  /**
   * An array of attributes, which should be return.
   *
   * Empty if all attributes.
   *
   * @var array
   */
  public $attributes = array();

  /**
   * A flag whether the returned data should be prepared.
   * @see DBPedia::prepared_data
   * @var Boolean
   */
  public $prepare = FALSE;

  /**
   * An array for the prepared Date.
   * - path
   *  Relative path, to link a annotation. If empty, the prepared content
   *  will be not linked.
   *  - class
   *  The css class, which should be added to the link.
   *
   * @var array
   */
  public $prepareOptions = array('path' => '', 'class' => '');

  /**
   * An array of types. Ex. Person, Land etc.
   *
   * @var array
   */
  public $types = array('Stadt', 'Person', 'Land', 'Organisation', 'Unternehmen', 'MediaUnternehmen');

  public $templates = array(
    'tpl1' => array(
      'location' => '',
      'types' => array('Person')),
    'tpl2' => array(
      'location' => '',
      'types' => array('Stadt')),
    'tpl3' => array(
      'location' => '',
      'types' => array('Land')),
    'tpl4' => array(
      'location' => '',
      'types' => array('Organisation', 'Unternehmen', 'MediaUnternehmen')),
  );

  /**
   *
   * Constructor
   * @param string $annotation
   * @param string $type
   * @return dbPedia
   */
  function __construct($annotation = '', $type = '') {
    $this->annotation = trim($annotation);
    $this->type = trim($type);
    if (!$this->validated()) {
      return $this;
    }
    return $this;
  }

  /**
   * Fetches the data and prepares the data, if necessary.
   * @return DBPedia
   */
  function run() {
    $this->fetchData();
    if (empty($this->response_data)) {
      return $this;
    }
    if (empty($this->attributes) && !empty($this->response_data['head']['vars'])) {
      $this->attributes = $this->response_data['head']['vars'];
    }
    foreach ($this->response_data['results']['bindings'] as $bind) {
      if (!empty($this->attributes)) {
        foreach ($this->attributes as $attr) {
          $new_val = $this->getAttr($attr, $bind);
          if (!empty($new_val)) {
            //initial
            if (empty($this->unprepared_data[$attr])) {
              $this->unprepared_data[$attr] = array($new_val['value']);
            }
            // Check existing data
            else {
              foreach ($this->unprepared_data[$attr] as $value) {
                $temp_new = NULL;
                if ($value === $new_val['value']) {
                  break;
                }
                $temp_new = $new_val['value'];
              }
              if (!empty($temp_new)) {
                $this->unprepared_data[$attr][] = $temp_new;
              }
            }
          }
        }
      }
    }
    if ($this->prepare) {
      $this->prepareValues();
    }
    return $this;
  }

  /**
   * Fetches the data from the QueryURL.
   *
   * @return dbPedia
   */
  function fetchData() {
    $this->response_data = $this->getCache($this->annotation, $this->type);
    if (empty($this->response_data)) {
      $url = $this->getQueryURL();
      if ($url) {
        $response = drupal_http_request($url);
        if (empty($response->error)) {
          $this->response_data = json_decode($response->data, true);
          if (empty($this->response_data['results']['bindings'][0])) {
            return $this;
          }
          cache_set("Dbpedia:{$this->annotation}{$this->type}", $this->response_data, 'cache', 60*60*24*7);
        }
        else{
          $this->errorlog($response->error, $response->code);
          return $this;
        }
      }
      else{
        $this->errorlog(sprintf('Got no URL for this annotation %s and type %s.', $this->annotation, $this->type));
        return $this;
      }
    }
    return $this;
  }

  /**
   * Prepare the unprepared data.
   */
  function prepareValues() {
    $setlink = FALSE;
    $class = 'dbpedia-link';
    $path = '';
    $unprepared_data = $this->unprepared_data;
    if (empty($unprepared_data)) {
      return;
    }
    if (!empty($this->prepareOptions['path'])) {
      $path = $this->prepareOptions['path'];
      $setlink = TRUE;
      if (!empty($this->prepareOptions['class'])) {
        $class = $this->prepareOptions['class'];
      }
    }
    foreach ($unprepared_data as $attr => $datasets) {
      foreach ($datasets as $datavalue) {
        if ($attr == 'leader') {
          $temp_data = array('title' => 'Regierungschef/Führungsperson(en)', 'content' => check_plain($datavalue));
          if ($setlink) {
            $options = array('query' => array('type' => 'Person'), 'attributes' => array('class' => array($class)), 'data-dbpediaType' => 'Person');
            $temp_data['content'] = l($datavalue, $path . $datavalue, $options);
          }
          if ($this->type == 'Stadt') {
            $temp_data['title'] = 'Bürgermeister';
          }
          $this->prepared_data[$attr][] = $temp_data;
        }
        elseif ($attr == 'population') {
          $this->prepared_data[$attr][] = array('title' => 'Einwohnerzahl', 'content' => check_plain(number_format($datavalue, 0, ",", ".")));
        }
        elseif ($attr == 'postalCode') {
          $temp = explode(',', $datavalue);
          if (!empty($temp)) {
            foreach ($temp as $k => $v) {
              $v = trim($v);
              if (empty($v)) {
                unset($temp[$k]);
              }
            }
          }
          array_filter($temp);
          if (!empty($temp)) {
            $temp_data = (count($temp) > 1) ? implode(', ', $temp) : $temp;
            $this->prepared_data[$attr][] = array('title' => 'Postleitzahl', 'content' => check_plain($temp_data));
            unset($temp);
          }
        }
        elseif ($attr == 'area' && $datavalue/1000000) {
          $this->prepared_data[$attr][] = array('title' => 'Fläche', 'content' => number_format($datavalue/1000000, 3, ",", ".") . ' km²');
        }
        elseif ($attr == 'bday') {
          $this->prepared_data[$attr][] = array('title' => 'Geburtsdatum', 'content' => format_date(strtotime($datavalue), 'custom', 'd.m.Y'));
        }
        elseif ($attr == 'bcity') {
          $temp_data = array('title' => 'Geburtsort', 'content' => check_plain($datavalue));
          if ($setlink) {
            $options = array('query' => array('type' => 'Stadt'), 'attributes' => array('class' => array($class)), 'data-dbpediaType' => 'Stadt');
            $temp_data['content'] = l($datavalue, $path . $datavalue, $options);
          }
          $this->prepared_data[$attr][] = $temp_data;
        }
        elseif ($attr == 'capital') {
          $temp_data = array('title' => 'Hauptstadt', 'content' => check_plain($datavalue));
          if ($setlink) {
            $options = array('query' => array('type' => 'Stadt'), 'attributes' => array('class' => array($class)), 'data-dbpediaType' => 'Stadt');
            $temp_data['content'] = l($datavalue, $path . $datavalue, $options);
          }
          $this->prepared_data[$attr][] = $temp_data;
        }
        elseif ($attr == 'formationDate') {
          $this->prepared_data[$attr][] = array('title' => 'Gegründet am:', 'content' => format_date(strtotime($datavalue), 'custom', 'd.m.Y'));
        }
        elseif ($attr == 'formationPlace') {
          $this->prepared_data[$attr][] = array('title' => 'Gegründet in:', 'content' => check_plain($datavalue));
        }
        elseif ($attr == 'foundedBy') {
          if (strpos($datavalue, 'http') !== FALSE ) {
            $paths = explode('/', $datavalue);
            $datavalue = array_pop($paths);
          }
          $datavalue = str_replace('_', ' ', $datavalue);
          $temp_data = array('title' => 'Gegründet von:', 'content' => check_plain($datavalue));
          if ($setlink) {
            $options = array('query' => array('type' => 'Person'), 'attributes' => array('class' => array($class)), 'data-dbpediaType' => 'Person');
            $temp_data['content'] = l($datavalue, $path . $datavalue, $options);
          }
          $this->prepared_data[$attr][] = $temp_data;
        }
        elseif ($attr == 'keyPerson') {
          if (strpos($datavalue, 'http') !== FALSE ) {
            $paths = explode('/', $datavalue);
              $datavalue = array_pop($paths);
          }
          $datavalue = str_replace('_', ' ', $datavalue);
          $temp_data = array('title' => 'Schlüsselperson', 'content' => check_plain($datavalue));
          if ($setlink) {
            $options = array('query' => array('type' => 'Person'), 'attributes' => array('class' => array($class)), 'data-dbpediaType' => 'Person');
            $temp_data['content'] = l($datavalue, $path . $datavalue, $options);
          }
          $this->prepared_data[$attr][] = $temp_data;
        }
        elseif ($attr == 'numberOfEmployees') {
          $this->prepared_data[$attr][] = array('title' => 'Mitarbeiteranzahl', 'content' => check_plain($datavalue));
        }
        elseif ($attr == 'homepage') {
          $this->prepared_data[$attr][] = array('title' => 'Homepage', 'content' => check_plain($datavalue));
        }
        elseif ($attr == 'thumbnail') {
          $this->prepared_data[$attr][] = array('title' => 'Bild', 'content' => '<img src="' . check_plain($datavalue) . '" />');
        }
        elseif ($attr == 'abstract') {
          $this->prepared_data[$attr][] = array('title' => 'Kurzbeschreibung', 'content' => check_markup($datavalue, 'filtered_html'));
        }
        else {
          $this->prepared_data[$attr][] = array('title' => 'n/a', 'content' => check_plain($datavalue));
        }
      }
    }
  }

  /**
   * Returns a single result set for a attribute.
   *.
   * @param string $attr
   * A attributeb like lat, longl.
   * @param array $bind
   * A result set.
   *
   * @see DBPedia::attributes
   *
   * @return array|NULL
   */
  function getAttr($attr, $bind = array()) {
    if (is_string($attr)) {
      return !empty($bind[$attr]) ? $bind[$attr] : NULL;
    }
    return NULL;
  }

  /**
   * Validates whether the annotation or type is empty and whether the type is
   * in list of types.
   *
   * @return boolean TRUE|FALSE
   *
   */
  function validated() {
    if (empty($this->annotation)) {
      $this->errorlog('Annotation is empty.');
      return FALSE;
    }
    if (empty($this->type)) {
      $this->errorlog('Type is empty.');
      return FALSE;
    }
    if (!in_array($this->type, $this->types)) {
      $this->errorlog('Type is not in the supported list.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   *
   * Returns the URL for a type.
   *
   * @return NULL|string
   *  - NULL if we found no URL for this type.
   *  - string if we found a URL for this type.
   */
  function getQueryURL() {
    $format = 'json';
    $term = urldecode($this->annotation);
    $type = $this->type;
    $tpl = $this->findTemplate($type);
    if (empty($tpl)) {
      return NULL;
    }
    $query = '';
    ob_start();
    include $tpl;
    $query = ob_get_clean();
    if (empty($query)) {
      return NULL;
    }

    $searchUrl = url('http://dbpedia.org/sparql', array('external' => true, 'query' => array(
      'query' => $query,
      'format' => $format
    )));
    return $searchUrl;
  }

  /**
   * Try to fetch the cached response data, which is the set in
   * DBPedia::fetchData.
   *
   * @param $annotation
   * The annotation, which is set in DBPedia::annotation.
   * @param $type
   * The annotation type, which is set in DBPedia::type.
   *
   * @return null|array
   * -null No cache found.
   * -array Found the cache.
   */
  function getCache($annotation, $type) {
    $cache = cache_get("Dbpedia:$annotation$type");
    return !empty($cache->data) ? $cache->data : NULL;
  }

  /**
   * Returns the path to the template for a type.
   *
   * @param string $type
   * @return null|string
   * The Path.
   */
  function findTemplate($type) {
    $filename = '';
    foreach ($this->templates as $tpl => $template) {
      if (in_array($type, $template['types'])) {
        if (empty($template['location'])) {
          $filename = dirname(__FILE__) . "/templates/$tpl.tpl.php";
        }
        else{
          $filename =  $template['location'] . "$tpl.tpl.php";
        }
        if (file_exists($filename)) {
          return $filename;
        }
      }
    }
    $this->errorlog(sprintf('Template %s for %s not found', $filename, $type));
    return NULL;
  }

  /**
     *
     * Throw a Exception if debug mode is enabled and log this error.
     *
     * @param string $msg
     * @param int|string $code
     * @throws Exception
    */
  function errorlog($msg = '', $code = '') {
    if (empty($msg)) {
      $msg = 'n/a';
    }
    if (empty($code)) {
      $code = 'n/a';
    }
    if ($this->debug) {
      watchdog('dbpedia', 'An error has occurred with the message %message and Code %code.', array('%message' => $msg, '%code' => $code), WATCHDOG_ERROR);
    }
  }
}

